<?php
/* @var $this yii\web\View */

use yii\helpers\Json;
use yii\helpers\Html;

$this->title = 'Home';
?>

<?php
echo Html::beginForm('index','post');
echo Html::input('text','cari-id');
echo ' '. Html::submitButton('Cari',['class'=>'submit']);
echo Html::endForm();

$decode = Json::decode($json);
$status = $decode['status'];

$variable = $decode['data'];
if ($variable == null ) {
	# code...
}else{
	foreach ($variable as $key => $value) {
		echo Html::tag('h1',$value['title']);
		echo Html::tag('p',Html::tag('small',$value['username'].' '.$value['date']));
	    echo $value['content'];
	    echo Html::tag('hr');
	}
	echo Html::tag('b',"Total Item : ". $decode['totalItems']);

}?>
