<?php

use yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">


    <div class="body-content">

        <?php foreach($post as $post){?>
        	<?php echo Html::tag('h1',$post->title);?>
        	<?php echo Html::tag('p',Html::tag('small',$post->username.' '.$post->date));?>
        	<?php echo $post->content;?>
        	<?php echo Html::tag('hr');?>
        <?php } ?>
    </div>
</div>
