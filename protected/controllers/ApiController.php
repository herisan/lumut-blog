<?php

namespace app\controllers;

use Yii;
use yii\db\Query;
use yii\helpers\BaseJson;
use yii\filters\ContentNegotiator;
use yii\filters\auth\HttpBasicAuth;
use yii\web\Response;

use app\models\Post;
use app\models\PostCategory;
use app\models\User;

class ApiController extends \yii\rest\Controller //jika hanya untuk CRUD pakai \yii\web\Controller
{
	public function behaviors()
	{
	    return [
	        'contentNegotiator' => [
				'class' => ContentNegotiator::className(),
				'formats' => [
					'application/json' => Response::FORMAT_JSON,
				],
			],
			'authenticator' => [
				'class' => HttpBasicAuth::className(),
				'auth' => function ($username, $password) {
					Yii::info("system attempts to login with '$username' and token '$password'", 'auth');
					return User::find()->where([
						'username' => $username,
						'password' => $password,
					])->one();
				}
			],
	    ];
	}
    public function actionIndex()
    {
    	$sort 		= "date DESC";
    	$query 		= new Query;
		$query->from('post')
			->orderBy($sort)
			->select("idpost,title,content,date,username");
		$command 	= $query->createCommand();
		$models 	= $command->queryAll();

		$totalItems = $query->count();
          
      	$this->setHeader(200);
        $status 	= 0;
        if ($models) {
         	$status = 1;
        } 
        $json = json_encode(array('status'=>$status,'data'=>$models,'totalItems'=>$totalItems),JSON_PRETTY_PRINT);
        return $this->render('index',['json'=>$json]);
    }

    public function actionGetPostAll()
    {
        \Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
		$post = Post::find()->orderBy('date DESC')->all();
		if(count($post) > 0 ) //check if record bigger than 0
		{
		return array('status' => true, 'data'=> $post);
		}
		else
		{
		return array('status' => false,'data'=> 'No Post Found');
		}
    }

    public function actionGetPostBy($id_post)
    {
    	\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
		$post = Post::find()->where(['idpost'=>$id_post])->one();
		if($post != null ) //check if record not null
		{
		return array('status' => true, 'data'=> $post);
		}
		else
		{
		return array('status' => false,'data'=> 'No Post Found');
		}
    }

    public function actionSetPost($id_post=null)
    {
    	if ($id_post != null) {
    		\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;     
	        $attributes = \yii::$app->request->post();
	       
	        $post = Post::find()->where(['idpost' => $id_post ])->one();
	        if($post )
	        {
	           $post->attributes = \yii::$app->request->post();
	           $post->save();
	           return array('status' => true, 'data'=> 'Post record is updated successfully');
	        }
	        else
	        {
	           return array('status' => false,'data'=> 'No Post Found');
	        }
    	}else{
	    	\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
			$post = new Post();
			$post->scenario = Post:: SCENARIO_CREATE;
			$post->attributes = \yii::$app->request->post();
			if($post->validate())
			{
				$post->save();
				return array('status' => true, 'data'=> 'Post record is successfully updated');
			}
			else
			{
				return array('status' => false,'data'=>$post->getErrors());    
			}
		}
    }

    public function actionSetCategory()
    {
    	\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
			$post = new PostCategory();
			$post->scenario = PostCategory:: SCENARIO_CREATE;
			$post->attributes = \yii::$app->request->post();
			if($post->validate())
			{
				$post->save();
				return array('status' => true, 'data'=> 'Post Category record is successfully updated');
			}
			else
			{
				return array('status' => false,'data'=>$post->getErrors());    
			}
    }

    public function actionSetDeletePostBy($id_post)
    {
		\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
		$attributes = \yii::$app->request->post();
	    $post = Post::find()->where(['idpost' => $id_post ])->one();  
      	if($post != null ) //check if record not null
		{
	       	$post->delete();
		  	return array('status' => true, 'data'=> 'Post record is successfully deleted'); 
      	}
		else
		{
			return array('status' => false,'data'=> 'No Student Found');
		}
    }

    private function setHeader($status)
	{
	  
	  $status_header = 'HTTP/1.1 ' . $status . ' ' . $this->_getStatusCodeMessage($status);
	  $content_type="application/json; charset=utf-8";

	  header($status_header);
	  header('Content-type: ' . $content_type);
	  header('X-Powered-By: ' . "Nintriva <nintriva.com>");
	}
	private function _getStatusCodeMessage($status)
	{
	  $codes = Array(
	  200 => 'OK',
	  400 => 'Bad Request',
	  401 => 'Unauthorized',
	  402 => 'Payment Required',
	  403 => 'Forbidden',
	  404 => 'Not Found',
	  500 => 'Internal Server Error',
	  501 => 'Not Implemented',
	  );
	  return (isset($codes[$status])) ? $codes[$status] : '';
	}	
	protected function findModel($id)
	{
	  if (($model = Post::findOne($id)) !== null) {
		  return $model;
	  } else {
		  
		$this->setHeader(400);
		echo json_encode(array('status'=>0,'error_code'=>400,'message'=>'Bad request'),JSON_PRETTY_PRINT);
		exit;
	  }
	}
}
